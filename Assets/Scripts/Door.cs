﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DemoAssets;

public class Door : MonoBehaviour {

	private Animator anim;

	void Start ()
	{
		anim = GetComponent<Animator>();
	}
	
	void OnTriggerStay2D(Collider2D coll)
	{
		if (coll.gameObject.CompareTag("Player") && coll.gameObject.GetComponent<Player> ().Interacting())
		{
			foreach (Door door in FindObjectsOfType<Door>())
				door.Open ();
		}
	}

	public void Open ()
	{
		anim.SetBool ("Fechada", !anim.GetBool("Fechada"));
	}
}
