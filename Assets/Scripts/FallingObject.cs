﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DemoAssets;

public class FallingObject : MonoBehaviour {
	bool HasFallen = false;

	public void OnCollisionEnter2D (Collision2D coll) {
		Fall (coll);
	}

	void Fall (Collision2D coll) {
		foreach (FallingObject fallObj in FindObjectsOfType<FallingObject>()) {
			if (!fallObj.HasFallen && coll.gameObject.CompareTag ("Tiro")) {
				fallObj.GetComponent<Rigidbody2D> ().constraints &= ~RigidbodyConstraints2D.FreezePositionY;
				fallObj.HasFallen = true;
			} else if (coll.gameObject.CompareTag ("Player") && (fallObj.GetComponent<Rigidbody2D> ().velocity.y < -0.1)) {
				Debug.Log (fallObj.GetComponent<Rigidbody2D> ().velocity.y);
				coll.gameObject.GetComponent<Player> ().TakeDamage (100);
			}
		}
	}
}