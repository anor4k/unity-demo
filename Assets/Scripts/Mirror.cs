﻿using System;
using UnityEngine;
using DemoAssets;

public class Mirror : MonoBehaviour {
	public GameObject objectToMirror;
	private Rigidbody2D rb;
	void Start () {
		rb = objectToMirror.GetComponent<Rigidbody2D>();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		Vector2 vel = gameObject.GetComponent<Rigidbody2D>().velocity;
		rb.velocity = vel;
		if (coll.gameObject.CompareTag("Player")) {
			Player player = coll.gameObject.GetComponent<Player> ();
			player.TakeDamage (vel.magnitude);
		}
	}
}
