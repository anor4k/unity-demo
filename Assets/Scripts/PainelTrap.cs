﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DemoAssets;

public class PainelTrap : MonoBehaviour {

	void OnTriggerStay2D (Collider2D coll) {
		
		if (coll.gameObject.CompareTag("Player") && coll.gameObject.GetComponent<Player> ().Interacting())
		{
			Activate ();
		}
	}

	void Activate () {
		foreach (Trap trap in FindObjectsOfType<Trap>()) {
			Animator anim = trap.GetComponent<Animator> ();
			anim.SetBool ("Ativo", !anim.GetBool ("Ativo"));
		}
	}
}
