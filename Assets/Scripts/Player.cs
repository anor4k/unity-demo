using System;
using UnityEngine;

namespace DemoAssets
{
    public class Player : MonoBehaviour
    {
		private float m_DamageTaken = 0;
		public bool m_dead = false;
		[SerializeField] private float m_MaxHealth = 100;
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private float m_JumpForce = 800f;                  // Amount of force added when the player jumps.
        [SerializeField] private bool m_AirControl = true;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

		public GameObject Tiro;
		public float fireRate = 0.25f;
		private float lastShot = 0.0f;
		private bool m_interacting = false;
        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;            // Whether or not the player is grounded.
        private Transform m_FiringPoint;   // A position marking where to check for ceilings
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
            m_FiringPoint = transform.Find("FiringPoint");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
        }


        private void FixedUpdate()
        {
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
				if (colliders [i].gameObject != gameObject)
					m_Grounded = true;
            }
        }

		public bool Interacting ()
		{
			return m_interacting;
		}

		public void Interact(bool interacting)
		{
			m_interacting = interacting;
		}

		public void TakeDamage(float damage) 
		{
			m_DamageTaken += damage;
			if (m_DamageTaken >= m_MaxHealth) {
				m_dead = true;
				m_Anim.SetBool ("Death", true);
			}
		}

		public void Shoot(bool shoot)
		{
			if (shoot && Time.time > fireRate + lastShot) {
				Tiro tiro = Instantiate (Tiro, m_FiringPoint.position, m_FiringPoint.rotation).GetComponent<Tiro> ();
				if (transform.localScale.x < 0) {
					tiro.speed *=-1;
				}
				tiro.GetComponent<Rigidbody2D> ().velocity = new Vector2 (tiro.speed, 0);
				lastShot = Time.time;
			}
		}

        public void Move(float move, bool jump)
        {
            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // The Speed animator parameter is set to the absolute value of the horizontal input.
                m_Anim.SetFloat("Speed", Mathf.Abs(move));

                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move*m_MaxSpeed, m_Rigidbody2D.velocity.y);

                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
                    // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
            }
            // If the player should jump...
            if (m_Grounded && jump)
            {
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            }
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
}
