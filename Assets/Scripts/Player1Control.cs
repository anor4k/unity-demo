﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using DemoAssets;

[RequireComponent(typeof (Player))]
public class Player1Control : MonoBehaviour {

	private Player m_Character;
	private bool m_Jump;


	private void Awake()
	{
		m_Character = GetComponent<Player>();
	}


	private void Update()
	{
		if (!m_Jump)
		{
			// Read the jump input in Update so button presses aren't missed.
			m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
		}
	}


	private void FixedUpdate()
	{
		if (m_Character.m_dead)
			return;
		// Read the inputs.
		bool interacting = Input.GetKeyDown(KeyCode.E);
		bool shoot = Input.GetKey (KeyCode.F);
		float h = CrossPlatformInputManager.GetAxis("Horizontal");
		// Pass all parameters to the character control script.
		m_Character.Move(h, m_Jump);
		m_Character.Shoot (shoot);
		m_Character.Interact (interacting);
		m_Jump = false;
	}
}
