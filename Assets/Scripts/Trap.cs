﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DemoAssets;

public class Trap : MonoBehaviour {
	
	void OnCollisionEnter2D (Collision2D coll) {
		coll.gameObject.GetComponent<Player> ().TakeDamage(120);
	}
}
